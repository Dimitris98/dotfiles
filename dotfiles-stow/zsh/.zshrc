#          _
#  _______| |__
# |_  / __| '_ \
#  / /\__ \ | | |
# /___|___/_| |_|

# zsh-autocomplete plugin. Must be on top of zshrc!
# source ~/.oh-my-zsh/custom/plugins/zsh-autocomplete/zsh-autocomplete.plugin.zsh

# zsh-syntax-higlighting settings.
ZSH_HIGHLIGHT_HIGHLIGHTERS=(main cursor)
typeset -gA ZSH_HIGHLIGHT_STYLES

# zsh-completions plugin.
fpath+=${ZSH_CUSTOM:-${ZSH:-~/.oh-my-zsh}/custom}/plugins/zsh-completions/src

# Autocomplete support.
autoload -U compinit; compinit

# Nix directory PATH.
# export PATH="$HOME/.nix-profile/bin:$PATH"

# Enable Powerlevel10k instant prompt. Should stay close to the top of zshrc.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" > /dev/null
  # > /dev/null is a workaround for % character pop up on top of the terminal (disable instant prompt).
fi

source ~/.oh-my-zsh/custom/themes/powerlevel10k/powerlevel10k.zsh-theme

# Support for 256 colors.
export TERM="xterm-256color"

# Make $HOME/.local/bin work globally.
export PATH="$HOME/.local/bin:$PATH"

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

# Use neovim as my default editor.
export EDITOR='nvim'

# Update oh-my-zsh automatically.
zstyle ':omz:update' mode auto
zstyle ':omz:update' frequency 1

# Load theme.
ZSH_THEME="powerlevel10k/powerlevel10k"

# Plugins list.
plugins=(eza git fzf sudo zsh-autosuggestions zsh-completions zsh-syntax-highlighting zoxide)

source $ZSH/oh-my-zsh.sh
source ~/.zshrc-alias
source ~/.zshrc-themes

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

# GPG git
export GPG_TTY=$TTY

##### Applications custom settings #####

# Better history search.
export HISTFILESIZE=100000
export HISTSIZE=100000
export SAVEHIST=100000
setopt HIST_EXPIRE_DUPS_FIRST
setopt HIST_FIND_NO_DUPS
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_SPACE

# Zoxide init.
eval "$(zoxide init zsh)"

# The Fuck alias.
# eval "$(thefuck --alias)"

# Pipx autocomplete.
# eval "$(register-python-argcomplete pipx)"

# npm settings.
# export PATH="$PATH:$HOME/.node/bin"

# DOOM Emacs
export PATH="$PATH:$HOME/.config/emacs/bin"

# dotnet settings.
# export DOTNET_CLI_TELEMETRY_OPTOUT=1
# export PATH="$PATH:$HOME/.dotnet/tools"

# deno source.
. "/var/home/$USER/.deno/env"

# Cargo bin path.
export PATH="$PATH:$HOME/.cargo/bin"

# Go bin path.
export PATH="$PATH:$HOME/go/bin"
