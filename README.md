<h1 align=center>🐧 Linux Dotfiles 🗃️</h1>

```{.bash}
git clone https://gitlab.com/Dimitris98/dotfiles.git
```

## About 🔍

This repository contains my personal dotfiles for my Linux systems, including shell scripts for various use cases (First time run setup, VSCode extensions list, etc).

## License 📜

- Licensed under [GPLv3](LICENSE)

---

_Repository icon created with [Emblem](https://gitlab.gnome.org/World/design/emblem) and Tux icon from [Wikimedia](https://commons.wikimedia.org/wiki/File:Icons8_flat_linux.svg)._
