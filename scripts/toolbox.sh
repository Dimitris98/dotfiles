#!/bin/bash

# Update Fedora Toolbox
echo "System update..."

sudo dnf update

# Install packages
echo "Install packages..."

packages=(aria2
	bat
	cargo
	duf
	eza
	fastfetch
	fd-find
	ffmpeg
	fzf
	go
	neovim
	openssl-devel # Zellij dependency
	perl          # Zellij dependency
	ranger
	ripgrep
	rust
	stow
	tealdeer
	'texlive-*' # Installs the complete texlive package, including all plugins
	texstudio
	zoxide
	zsh)

# Setup golang and install packages.
goPkgs() {

	# Disable telemetry by default.
	go telemetry off

	# Install lazydocker & lazygit.
	go install github.com/jesseduffield/lazydocker@latest
	go install github.com/jesseduffield/lazygit@latest

}

# Install Zellij via cargo.
cargoPkgs() {

	cargo install --locked zellij

}

# Install AstroNvim.
astroNvim() {

	git clone --depth 1 https://github.com/AstroNvim/template ~/.config/nvim
	rm -rf ~/.config/nvim/.git
	nvim

}

sudo dnf install $packages -y

goPkgs
cargoPkgs
astroNvim

echo "Setup complete"
exit
