#!/bin/bash

# This is a shell script designed for my freshly installed Arch based systems.

# Font settings.

RED='\033[0;31m'
NC='\033[0m' # No Color
BOLD=$(tput bold)

echo "
    _             _       ____ _______        __
   / \   _ __ ___| |__   | __ )_   _\ \      / /
  / _ \ | '__/ __| '_ \  |  _ \ | |  \ \ /\ / /
 / ___ \| | | (__| | | | | |_) || |   \ V  V /
/_/   \_\_|  \___|_| |_| |____/ |_|    \_/\_/

"

echo -e "${BOLD}Welcome!${NC}"

# List of packages.

gnomePackages=(aria2
	audacity
	bat
	bleachbit
	btop
	celluloid
	dconf-editor
	#dnsmasq
	duf
	eza
	fd
	figlet
	fzf
	#gdb
	gedit-plugins
	gparted
	gufw
	#iptables
	jre-openjdk
	kitty
	kvantum
	lazygit
	libreoffice-fresh
	man-db
	man-pages
	meld
	mpv
	neofetch
	neovim
	nix
	noto-fonts
	noto-fonts-emoji
	nvtop
	p7zip
	piper
	podman
	podman-compose
	podman-docker
	qbittorrent
	qemu-full
	ranger
	ripgrep
	signal-desktop
	telegram-desktop
	thefuck
	tldr
	tmux
	tree
	ufw
	unarchiver
	unrar
	vde2
	virt-manager
	yt-dlp
	zoxide
	zsh)

kdePackages=( #neochat
	aria2
	audacity
	bat
	bleachbit
	btop
	digikam
	#dnsmasq
	duf
	eza
	fd
	figlet
	filelight
	flameshot
	fzf
	#gdb
	gwenview
	#iptables
	jre-openjdk
	kamoso
	kate
	kcalc
	kdenlive
	kdepim-addons
	kget
	kitty
	krita
	lazygit
	libreoffice-fresh
	man-db
	man-pages
	meld
	merkuro
	mpv
	neofetch
	neovim
	nix
	noto-fonts
	noto-fonts-emoji
	okular
	p7zip
	partitionmanager
	piper
	podman
	podman-compose
	podman-docker
	qbittorrent
	qemu-full
	ranger
	ripgrep
	signal-desktop
	sqlitebrowser
	telegram-desktop
	thefuck
	tldr
	tmux
	tree
	ufw
	unarchiver
	unrar
	unzip
	vde2
	virt-manager
	vlc
	yt-dlp
	zip
	zoxide
	zsh)

flatpaks=( #com.github.johnfactotum.Foliate
	#org.gnome.Solanum
	#org.js.nuclear.Nuclear
	#org.libretro.RetroArch
	#com.github.IsmaelMartinez.teams_for_linux
	com.discordapp.Discord
	com.github.huluti.Curtail
	com.github.jeromerobert.pdfarranger
	com.github.tchx84.Flatseal
	com.github.unrud.VideoDownloader
	com.github.wwmm.easyeffects
	com.heroicgameslauncher.hgl
	com.mattjakeman.ExtensionManager
	com.sindresorhus.Caprine
	com.spotify.Client
	com.usebottles.bottles
	dev.geopjr.Collision
	fr.romainvigier.MetadataCleaner
	io.freetubeapp.FreeTube
	io.github.seadve.Mousai
	io.github.Soundux
	net.cozic.joplin_desktop
	org.gimp.GIMP
	org.gnome.World.PikaBackup
	org.kde.krita
	org.prismlauncher.PrismLauncher)

terminal=(asciiquarium
	jp2a
	cmatrix
	cowsay
	figlet
	lolcat
	sl)

gaming=(gamemode
	libretro-core-info
	lutris
	retroarch
	retroarch-assets-ozone
	retroarch-assets-xmb
	steam
	streamlink
	wine-mono
	wine-staging
	winetricks)

development=(cmake
	jdk-openjdk
	llvm
	npm
	python-pip
	python-pipx
	qtcreator
	sqlitebrowser)

remove=(epiphany
	gnome-books
	gnome-boxes
	gnome-maps
	gnome-music
	simple-scan
	totem)

aur=( #cava
      #lib32-mangohud
      #mangohud
      quickemu
      simplescreenrecorder
      ttf-ms-fonts
      vscodium-bin)

# Download and install all my core apps and packages from various repos and Flathub, depending on the specific desktop environment (GNOME or KDE).

echo "Are you using GNOME or KDE? [G/K]"
read de

if [[ $de =~ ^[Gg]+$ || $de == "" ]]; then

	echo "Update preinstalled packages and System..."

	sudo pacman -Syyu -y

	echo "Download and install apps..."

	# Download and install apps for the GNOME DE.

	sudo pacman -S "${gnomePackages[@]}" -y

	# Uninstall all preinstalled GNOME apps that I don't use.

	sudo pacman -Rsu "${remove[@]}"

	# Enable libvirt service.

	sudo systemctl enable --now libvirtd

	# Add current user to the libvirt group.

	sudo usermod -G libvirt -a $(whoami)

	# Nix daemon is launched at boot time.

	sudo systemctl enable nix-daemon.service

	# Add current user to nix-users group.

	sudo usermod -a -G nix-users $(whoami)

	# Nix add unstable channel and update it.

	nix-channel --add https://nixos.org/channels/nixpkgs-unstable
	nix-channel --update

	# Download apps from AUR.

	yay -S "${aur[@]}"

	# Download apps from Flathub.

	flatpak install flathub --user "${flatpaks[@]}" -y

elif [[ $de =~ ^[Kk]+$ ]]; then

	echo "Update preinstalled packages and System..."

	sudo pacman -Syyu -y

	echo "Download and install apps..."

	# Download and install apps for the KDE DE.

	sudo pacman -S "${kdePackages[@]}" -y

	# Enable libvirt service.

	sudo systemctl enable --now libvirtd

	# Add current user to the libvirt group.

	sudo usermod -G libvirt -a $(whoami)

	# Nix daemon is launched at boot time.

	sudo systemctl enable nix-daemon.service

	# Add current user to nix-users group.

	sudo usermod -a -G nix-users $(whoami)

	# Nix add unstable channel and update it.

	nix-channel --add https://nixos.org/channels/nixpkgs-unstable
	nix-channel --update

	# Download apps from AUR.

	yay -S "${aur[@]}"

	# Download apps from Flathub.

	flatpak install flathub --user "${flatpaks[@]}" -y

else

	echo "I'm sorry, support is only available for GNOME and KDE!"

fi

# Install extra packages for gaming, development or terminal.

echo "Would you like to install game utilities? [Y/N]"
read games

if [[ $games =~ ^[Yy]+$ || $games == "" ]]; then

	sudo pacman -S "${gaming[@]}" -y

fi

echo "Would you like to install development tools? [Y/N]"
read dev

if [[ $dev =~ ^[Yy]+$ || $dev == "" ]]; then

	sudo pacman -S "${development[@]}" -y

fi

echo "Would you like to install terminal tools? [Y/N]"
read term

if [[ $term =~ ^[Yy]+$ || $term == "" ]]; then

	sudo pacman -S "${terminal[@]}" -y

fi

# Download Numix Circle Icon Pack, Papirus Icon Pack and Dracula GTK Theme from Github.

echo "Would you like to setup desktop & icon theming? [Y/N]"
read theme

if [[ $theme =~ ^[Yy]+$ || $theme == "" ]]; then

	echo "Download & install icons-themes"

	# Create icons and themes folders.
	mkdir /home/$(whoami)/.icons
	mkdir /home/$(whoami)/.themes
	cd /home/$(whoami)/Downloads
	# Download Numix Circle Icons.
	git clone https://github.com/numixproject/numix-icon-theme-circle.git
	mv /home/$(whoami)/Downloads/numix-icon-theme-circle/Numix-Circle /home/$(whoami)/.icons
	rm -r -f /home/$(whoami)/Downloads/numix-icon-theme-circle
	# Download Papirus Icons.
	git clone https://github.com/PapirusDevelopmentTeam/papirus-icon-theme.git
	mv /home/$(whoami)/Downloads/papirus-icon-theme/Papirus /home/$(whoami)/.icons
	rm -r -f /home/$(whoami)/Downloads/papirus-icon-theme
	# Download GTK Dracula Theme and Cursor.
	git clone https://github.com/dracula/gtk.git
	mv /home/$(whoami)/Downloads/gtk /home/$(whoami)/.themes/Dracula
	mv /home/$(whoami)/.themes/Dracula/kde/cursors/Dracula-cursors /home/$(whoami)/.icons/Dracula-cursors
	cd ~/

	# Apply theming on all flatpak apps.
	sudo flatpak override --filesystem=~/.themes
	# Choose the Dracula theme.
	sudo flatpak override --env=GTK_THEME=Dracula

fi

# Download Nerd fonts.

echo "Would you like to setup fonts? [Y/N]"
read fonts

if [[ $fonts =~ ^[Yy]+$ || $fonts == "" ]]; then

	echo "Downlaod & install Nerd fonts"

	# Create fonts folder.
	mkdir /home/$(whoami)/.fonts
	cd /home/$(whoami)/Downloads
	# Download fonts.
	wget https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Regular.ttf https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold.ttf https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Italic.ttf https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold%20Italic.ttf
	cd ~/

fi

# Download and install oh-my-zsh.

echo "Would you like to setup oh-my-zsh and install extra plugins? [Y/N]"
read omz

if [[ $omz =~ ^[Yy]+$ || $omz == "" ]]; then

	echo "Install oh-my-zsh"
	sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended

	# Download zsh plugins.

	echo "Download & install zsh plugins"

	# Powerlevel10k plugin.
	git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
	# Autosuggestions plugin.
	git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
	# Autocomplete plugin.
	cd /home/$(whoami)/.oh-my-zsh/plugins
	git clone --depth 1 -- https://github.com/marlonrichert/zsh-autocomplete.git
	# Highlight plugin.
	git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
	cd ~/
	# Make zsh the default shell.
	chsh -s $(which zsh)

fi

# Copy the config files from repo folder to home folder.

# echo "Copy configs from repo to home folder"

# rm ~/.config/gtk-4.0
# mv ~/dotfiles/config/gtk-4.0 ~/.config/gtk-4.0/gtk.css
# mv ~/dotflies/config/kitty ~/.config/kitty/kitty.conf
# rm ~/.zshrc
# mv ~/dotfiles/config/zsh/.zshrc ~/
# mv ~/dotfiles/config/p10k/.p10k.zsh ~/
# mv ~/dotfiles/config/tmux/.tmux.conf ~/
# mv ~/dotfiles/config/neofetch/.config.conf ~/.config/neofetch/.config.conf
# mv ~/dotfiles/config/MangoHud/MangoHud.conf ~/.config/MangoHud/MangoHud.conf

# Download and install AstroNvim from Github.

echo "Would you like to install AstroNvim? [Y/N]"
read AstroNvim

if [[ $AstroNvim =~ ^[Yy][Ee][Ss]$ || $AstroNvim == "" ]]; then

	echo "Install AstroNvim..."

	git clone --depth 1 https://github.com/AstroNvim/template ~/.config/nvim
        rm -rf ~/.config/nvim/.git

fi

echo -e "${BOLD}Setup complete!"
exit
