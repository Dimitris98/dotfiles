echo "
__     ______   ____ ___  ____ ___ _   _ __  __
\ \   / / ___| / ___/ _ \|  _ \_ _| | | |  \/  |
 \ \ / /\___ \| |  | | | | | | | || | | | |\/| |
  \ V /  ___) | |__| |_| | |_| | || |_| | |  | |
   \_/  |____/ \____\___/|____/___|\___/|_|  |_|"

echo "Installing extensions..."

codium --install-extension Angular.ng-template
codium --install-extension bradlc.vscode-tailwindcss
codium --install-extension Catppuccin.catppuccin-vsc
codium --install-extension dbaeumer.vscode-eslint
codium --install-extension dracula-theme.theme-dracula
codium --install-extension esbenp.prettier-vscode
codium --install-extension foxundermoon.shell-format
codium --install-extension jnoortheen.nix-ide
codium --install-extension ms-azuretools.vscode-docker
codium --install-extension ms-python.python
codium --install-extension ms-vscode-remote.remote-containers
codium --install-extension ms-vscode.cpptools
codium --install-extension PKief.material-icon-theme
codium --install-extension ritwickdey.LiveServer
codium --install-extension streetsidesoftware.code-spell-checker
codium --install-extension tamasfe.even-better-toml
codium --install-extension vscjava.vscode-java-pack
codium --install-extension vscodevim.vim