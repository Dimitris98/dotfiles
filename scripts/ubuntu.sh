#!/bin/bash

# This is a shell script designed for my freshly installed Pop!_OS / Ubuntu based systems.

# Font settings.

RED='\033[0;31m'
NC='\033[0m' # No Color
BOLD=$(tput bold)

echo "
 _   _ _                 _
| | | | |__  _   _ _ __ | |_ _   _
| | | | '_ \| | | | '_ \| __| | | |
| |_| | |_) | |_| | | | | |_| |_| |
 \___/|_.__/ \__,_|_| |_|\__|\__,_|
"

echo -e "${BOLD}Welcome!${NC}"

apps=( #lazygit
	aria2
	bat
	bleachbit
	bridge-utils
	celluloid
	cheese
	dconf-editor
	duf
	#eza
	fd-find
	ffmpeg
	fzf
	gedit-plugins
	git
	gnome-shell-extensions
	gnome-tweaks
	gparted
	gufw
	kitty
	libreoffice
	libvirt-clients
	libvirt-daemon-system
	meld
	neofetch
	nvtop
	piper
	podman
	qemu-kvm
	qt5-style-kvantum
	qt5-style-kvantum-themes
	ranger
	simplescreenrecorder
	telegram-desktop
	tmux
	ubuntu-restricted-extras
	virt-manager
	zoxide
	zsh)

flatpaks=( #com.github.johnfactotum.Foliate
	#org.gnome.Solanum
	#org.js.nuclear.Nuclear
	#org.libretro.RetroArch
	# com.github.IsmaelMartinez.teams_for_linux
	com.discordapp.Discord
	com.github.huluti.Curtail
	com.github.jeromerobert.pdfarranger
	com.github.tchx84.Flatseal
	com.github.unrud.VideoDownloader
	com.github.wwmm.easyeffects
	com.heroicgameslauncher.hgl
	com.mattjakeman.ExtensionManager
	com.sindresorhus.Caprine
	com.spotify.Client
	com.usebottles.bottles
	dev.geopjr.Collision
	fr.romainvigier.MetadataCleaner
	io.freetubeapp.FreeTube
	io.github.seadve.Mousai
	io.github.Soundux
	net.cozic.joplin_desktop
	org.gimp.GIMP
	org.gnome.World.PikaBackup
	org.kde.krita
	org.prismlauncher.PrismLauncher
	org.qbittorrent.qBittorrent)

terminal=(cava
	cmatrix
	cowsay
	figlet
	jp2a
	lolcat
	sl)

gaming=(gamemode
	goverlay
	mangohud
	steam
	streamlink
	winetricks)

development=(cmake
	default-jdk
	llvm
	npm
	pip
	pipx
	sqlite3
	sqlitebrowser)

# Flatpak Support for Ubuntu ONLY!

echo -e "Would you like to add flatpak support? ${RED}${BOLD}(Ubuntu ONLY) ${NC}[Y/N]"
read flatpak

if [[ $flatpak =~ ^[Yy]+$ || $flatpak == "" ]]; then

	sudo apt update && sudo apt install flatpak gnome-software-plugin-flatpak -y
	flatpak remote-add --if-not-exists --user flathub https://flathub.org/repo/flathub.flatpakrepo
	echo -e "${RED}${BOLD}To complete setup, restart your system!${NC}"
fi

# Full system update.

echo "Update system..."

sudo apt update && sudo apt full-upgrade -y

# Download and install XanMod Kernel.

echo "Would you like to install XanMod Kernel? [Y/N]"
read kernel

if [[ $kernel =~ ^[Yy]+$ || $kernel == "" ]]; then

	echo 'deb http://deb.xanmod.org releases main' | sudo tee /etc/apt/sources.list.d/xanmod-kernel.list
	wget -qO - https://dl.xanmod.org/gpg.key | sudo apt-key --keyring /etc/apt/trusted.gpg.d/xanmod-kernel.gpg add -
	sudo apt update && sudo apt install linux-xanmod-x64v3 -y
fi

# Download and install all my core apps and packages from various repos and Flathub.

echo "Install apps"

# Download apps from default repo.
sudo apt install "${apps[@]}" -y

# Download apps from Flathub.
flatpak install flathub --user "${flatpaks[@]}" -y

# Add Nix package manager.
sh <(curl -L https://nixos.org/nix/install) --daemon

# Install extra packages for gaming, development or terminal.

echo "Would you like to install game utilities? [Y/N]"
read games

if [[ $games =~ ^[Yy]+$ || $games == "" ]]; then

	sudo pacman -S "${gaming[@]}" -y

fi

echo "Would you like to install development tools? [Y/N]"
read dev

if [[ $dev =~ ^[Yy]+$ || $dev == "" ]]; then

	sudo pacman -S "${development[@]}" -y

fi

echo "Would you like to install terminal tools? [Y/N]"
read term

if [[ $term =~ ^[Yy]+$ || $term == "" ]]; then

	sudo pacman -S "${terminal[@]}" -y

fi

# Download Numix Circle Icon Pack, Papirus Icon Pack and Dracula GTK Theme from Github.

echo "Would you like to setup desktop & icon theming? [Y/N]"
read theme

if [[ $theme =~ ^[Yy]+$ || $theme == "" ]]; then

	echo "Install icons-themes..."

	# Create icons and themes folders.
	mkdir /home/$(whoami)/.icons
	mkdir /home/$(whoami)/.themes
	cd /home/$(whoami)/Downloads
	# Download Numix Circle Icons.
	git clone https://github.com/numixproject/numix-icon-theme-circle.git
	mv /home/$(whoami)/Downloads/numix-icon-theme-circle/Numix-Circle /home/$(whoami)/.icons
	rm -r -f /home/$(whoami)/Downloads/numix-icon-theme-circle
	# Download Papirus Icons.
	git clone https://github.com/PapirusDevelopmentTeam/papirus-icon-theme.git
	mv /home/$(whoami)/Downloads/papirus-icon-theme/Papirus /home/$(whoami)/.icons
	rm -r -f /home/$(whoami)/Downloads/papirus-icon-theme
	# Download GTK Dracula Theme and Cursor.
	git clone https://github.com/dracula/gtk.git
	mv /home/$(whoami)/Downloads/gtk /home/$(whoami)/.themes/Dracula
	mv /home/$(whoami)/.themes/Dracula/kde/cursors/Dracula-cursors /home/$(whoami)/.icons/Dracula-cursors
	cd ~/

	# Apply the theme to all Flatpak apps.
	sudo flatpak override --filesystem=~/.themes
	# Choose the Dracula theme.
	sudo flatpak override --env=GTK_THEME=Dracula

fi

# Download Nerd fonts.

echo "Would you like to setup fonts? [Y/N]"
read fonts

if [[ $fonts =~ ^[Yy]+$ || $fonts == "" ]]; then

	echo "Download & install Nerd fonts..."

	# Create fonts folder.
	mkdir /home/$(whoami)/.fonts
	cd /home/$(whoami)/Downloads
	# Download fonts.
	wget https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Regular.ttf https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold.ttf https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Italic.ttf https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold%20Italic.ttf
	cd ~/

fi

# Download and install oh-my-zsh.

echo "Would you like to setup oh-my-zsh and install extra plugins? [Y/N]"
read omz

if [[ $omz =~ ^[Yy]+$ || $omz == "" ]]; then

	echo "Install oh-my-zsh..."

	sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended

	# Download zsh plugins.

	echo "Download & install zsh plugins..."

	# Powerlevel10k plugin.
	git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
	# Autosuggestions plugin.
	git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
	# Autocomplete plugin.
	cd /home/$(whoami)/.oh-my-zsh/plugins
	git clone --depth 1 -- https://github.com/marlonrichert/zsh-autocomplete.git
	# Highlight plugin.
	git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
	cd ~/
	# Make zsh the default shell.
	chsh -s $(which zsh)

fi

# Copy the config files from repo folder to home folder.

# echo "Copy configs from repo to home folder"

# rm ~/.config/gtk-4.0
# mv ~/dotfiles/config/gtk-4.0 ~/.config/gtk-4.0/gtk.css
# mv ~/dotflies/config/kitty ~/.config/kitty/kitty.conf
# rm ~/.zshrc
# mv ~/dotfiles/config/zsh/.zshrc ~/
# mv ~/dotfiles/config/p10k/.p10k.zsh ~/
# mv ~/dotfiles/config/tmux/.tmux.conf ~/
# mv ~/dotfiles/config/neofetch/.config.conf ~/.config/neofetch/.config.conf
# mv ~/dotfiles/config/MangoHud/MangoHud.conf ~/.config/MangoHud/MangoHud.conf

# Download and install Neovim Stable.

echo "Would you like to install NeoVim Stable? [Y/N]"
read neovim

if [[ $neovim =~ ^[Yy]+$ || $neovim == "" ]]; then

	# Add Neovim ppa.
	sudo add-apt-repository ppa:neovim-ppa/stable
	# Download and install Neovim Stable.
	sudo apt update && sudo apt install neovim -y

fi

# Download and install AstroNvim from Github.

echo "Would you like to install AstroNvim? [Y/N]"
read AstroNvim

if [[ $AstroNvim =~ ^[Yy][Ee][Ss]$ || $AstroNvim == "" ]]; then

	echo "Install AstroNvim..."

	git clone --depth 1 https://github.com/AstroNvim/template ~/.config/nvim
        rm -rf ~/.config/nvim/.git

fi

# Download and install Quickemu from Github.

echo "Would you like to install Quickemu? [Y/N]"
read quickemu

if [[ $quickemu =~ ^[Yy]+$ || $quickemu == "" ]]; then

	# Add Quickemu ppa.
	sudo apt-add-repository ppa:flexiondotorg/quickemu
	# Download and install Quickemu.
	sudo apt update && sudo apt install quickemu -y

fi

# Download and install Wine-staging.

echo "Would you like to install Wine-Staging? [Y/N]"
read wine

if [[ $wine =~ ^[Yy]+$ || $wine == "" ]]; then

	# If your system is 64 bit, enable 32 bit architecture (if you haven't already).
	sudo dpkg --add-architecture i386
	# Download and add the repository key.
	sudo mkdir -pm755 /etc/apt/keyrings
	sudo wget -O /etc/apt/keyrings/winehq-archive.key https://dl.winehq.org/wine-builds/winehq.key
	# Add the repository for Ubuntu 22.04.
	sudo wget -NP /etc/apt/sources.list.d/ https://dl.winehq.org/wine-builds/ubuntu/dists/jammy/winehq-jammy.sources
	# Install Wine-Staging.
	sudo apt update && sudo apt install --install-recommends winehq-staging -y

fi

# Download and install VSCodium.

echo "Would you like to install VSCodium? [Y/N]"
read code

if [[ $code =~ ^[Yy]+$ || $code == "" ]]; then

	# Add the GPG key of the repository:
	wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg |
		gpg --dearmor |
		sudo dd of=/usr/share/keyrings/vscodium-archive-keyring.gpg
	# Add the repository:
	echo 'deb [ signed-by=/usr/share/keyrings/vscodium-archive-keyring.gpg ] https://download.vscodium.com/debs vscodium main' |
		sudo tee /etc/apt/sources.list.d/vscodium.list
	# Update then install vscodium:
	sudo apt update && sudo apt install codium -y

fi

echo -e "${BOLD}Setup complete"
exit
