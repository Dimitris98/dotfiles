#!/bin/bash

# Flatpaks list.
flatpaks=(
	# app.devsuite.Ptyxis
	# org.gnome.Boxes
	# org.cryptomator.Cryptomator
	com.discordapp.Discord
	com.github.huluti.Curtail
	com.github.jeromerobert.pdfarranger
	com.github.tchx84.Flatseal
	com.github.unrud.VideoDownloader
	com.github.wwmm.easyeffects
	com.mattjakeman.ExtensionManager
	com.spotify.Client
	com.vscodium.codium
	dev.geopjr.Collision
	fr.romainvigier.MetadataCleaner
	io.bassi.Amberol
	io.freetubeapp.FreeTube
	io.github.celluloid_player.Celluloid
	io.github.seadve.Mousai
	io.github.Soundux
	io.gitlab.news_flash.NewsFlash
	io.missioncenter.MissionCenter
	io.podman_desktop.PodmanDesktop
	it.mijorus.gearlever
	net.cozic.joplin_desktop
	org.gimp.GIMP
	org.gnome.World.PikaBackup
	org.libreoffice.LibreOffice
	org.localsend.localsend_app
	org.mozilla.firefox
	org.mozilla.Thunderbird
	org.qbittorrent.qBittorrent
)

# RPM package list.
packages=(
	distrobox
	eza
	fzf
	git
	gnome-boxes
	gnome-tweaks
	p7zip
	p7zip-plugins
	stow
	zoxide
	zsh
)

# Install flatpaks.
installFlatpakApps() {

	echo "Installing applications via Flathub. Please wait while the installation completes..."
	flatpak install flathub "${flatpaks[@]}" -y
	echo "Flatpaks installed successfully!"

}

# Update core system packages.
prepareSystem() {

	echo "System update in progress. Please wait while the updates are being applied..."
	rpm-ostree upgrade
	echo "Update completed successfully!"

	echo "Installing required packages. Please wait while the installation completes..."
	rpm-ostree install "${packages[@]}" -y
	echo "Packages installed successfully!"

	echo "Install oh-my-zsh..."
	sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

}

# Install dev containers using distrobox and toolbox.
setupContainers() {

	distrobox create --image ubuntu:latest --additional-packages "bat crun fd-find fzf git neofetch podman tldr tree zoxide" --name LocalAI
	toolbox create devcontainer

}

shellSetup() {

	echo "Download & install zsh plugins..."
	# Powerlevel10k plugin.
	git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k

	# Autosuggestions plugin.
	git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions

	# Autocomplete plugin.
	cd /home/$(whoami)/.oh-my-zsh/plugins
	git clone --depth 1 -- https://github.com/marlonrichert/zsh-autocomplete.git

	# Highlight plugin.
	git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
	cd ~/

	# Make zsh the default shell.
	chsh -s $(which zsh)

}

moveDotfiles() {

	stow -t ~/ nano zsh
	stow -t ~/.config/tmux tmux

}

prepareSystem
installFlatpakApps
setupContainers
moveDotfiles
shellSetup
